// Mersenne Twister random number generator (32-bit)
// Input: S N
// S - 32-bit seeding integer
// N - Total amount of numbers to generate
#include <iostream>
#include <cstdint>


const uint8_t   word_size           = 32;
const uint16_t  recurrence_degree   = 624;
uint16_t        middle_word         = 397;
const uint8_t   separation_point    = 31;
const uint32_t  rnftm_coefficient   = 0x9908B0DF;

const uint8_t   add_tempering_bit_u = 11;
const uint32_t  add_tempering_bit_d = 0xFFFFFFFF;
const uint8_t   add_tempering_bit_l = 18;

const uint32_t  tempering_bitmsk_b  = 0x9D2C5680;
const uint32_t  tempering_bitmsk_c  = 0xEFC60000;

const uint8_t   tempering_bitsft_s  = 7;
const uint8_t   tempering_bitsft_t  = 15;

const uint32_t  mask_lower          = ((1ULL << separation_point) - 1);
const uint32_t  mask_upper          = ~ mask_lower;

const uint32_t  initialisation_f    = 1812433253;
const uint32_t  default_seed        = 5489;


uint32_t        state[recurrence_degree]; // Elements of size word_size
uint16_t        current             = (recurrence_degree + 1);


void mt_initialise(uint32_t seed){
    current = recurrence_degree;
    state[0] = seed;
    for (int i = 1; i < recurrence_degree; ++i){
        state[i] = (initialisation_f * (state[i - 1] ^ (state[i - 1] >> (word_size - 2))) + i);
    }
}

void mt_twist(){
    for (int i = 0; i < recurrence_degree; ++i){
        uint32_t x   = ((state[i] & mask_upper) + (state[(i+1) % recurrence_degree] & mask_lower));
        uint32_t _x  = (x >> 1);

        if ((x % 2) != 0){
            _x = (_x ^ rnftm_coefficient);
        }

        state[i] = (state[(i + middle_word) % recurrence_degree] ^ _x);

        current = 0;
    }
}

uint32_t mt_next(){
    if (current >= recurrence_degree){
        if (current > recurrence_degree){
            mt_initialise(default_seed);
        }

        mt_twist();
    }

    uint32_t x = state[current];

    x = (x ^ ((x >> add_tempering_bit_u) & add_tempering_bit_d));
    x = (x ^ ((x << tempering_bitsft_s) & tempering_bitmsk_b));
    x = (x ^ ((x << tempering_bitsft_t) & tempering_bitmsk_c));
    x = (x ^ (x >> add_tempering_bit_l));

    ++current;
    return x;
}


int main(int argc, char* argv[]){
    uint32_t S = 0;
    int N = 0;
    std::cin >> S;
    std::cin >> N;

    mt_initialise(S);

    for (int i = 0; i < N; ++i){
        std::cout << mt_next() << std::endl;
    }

    return 0;
}
