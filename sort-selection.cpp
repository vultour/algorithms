// Selection sort
// Input: N X1 [X2 ... XN]
#include <iostream>


void selection_sort(int x[], int N){
    if (N < 2){ return; }
    for (int i = 0; i < (N - 1); ++i){
        int min = i;

        for (int j = (i + 1); j < N; ++j){
            if (x[j] < x[min]){
                min = j;
            }
        }

        int tmp = x[i];
        x[i]    = x[min];
        x[min]  = tmp;
    }
}

int main(int argc, char* argv[]){
    int N = 0;
    std::cin >> N;

    int e[N];
    for (int i = 0; i < N; ++i){ std::cin >> e[i]; }

    selection_sort(e, N);
    for (int i = 0; i < N; ++i){ std::cout << " " << e[i]; }
    std::cout << std::endl;

    return 0;
}
