// Fibonacci number generator
// Input: N
#include <iostream>


void fibonacci(unsigned int x, unsigned int y, int N){
    if (N < 1){ return; }

    int next = (x + y);
    std::cout << " " << next;

    fibonacci(y, next, (N - 1));
}

int main(int argc, char* argv[]){
    int N = 0;
    std::cin >> N;

    if (N < 1){ return 0; }
    else if (N < 2){ std::cout << " 0"; }
    else{ std::cout << " 0 1"; }

    fibonacci(0, 1, (N - 1));
    std::cout << std::endl;

    return 0;
}
