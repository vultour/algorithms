#!/usr/bin/env python
import sys

def fibonacci(x):
    if x <= 0: return 0
    if x == 1: return 1

    return fibonacci(x-1) + fibonacci(x-2)


if __name__ == "__main__":
    for i in range(10):
        print(fibonacci(i))

