#!/usr/bin/env python

# Given an array, finds the point at which the sum of both sides of the array is equal
def find_split_point(a):
    total = sum(a)
    x = 0
    for index, value in enumerate(a):
        x = x + a[index]
        total = total - a[index]
        if total == x: return index
    return -1


if __name__ == "__main__":
    a = [1, 2, 3, 4, 5, 6, 7, 2]
    point = find_split_point(a) + 1
    print(a)
    print("{}: {} - {}".format(point - 1, a[:point], a[point:]))

