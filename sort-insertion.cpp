// Insertion sort
// Input: N X1 [X2 .. XN]
#include <iostream>


void insertion_sort(int x[], int N){
    for (int i = 1; i < N; ++i){
        int j = i;
        int tmp = x[i];

        while ((j > 0) && (x[j - 1] > tmp)){
            x[j] = x[j - 1];
            --j;
        }

        x[j] = tmp;
    }
}

int main(int argc, char* argv[]){
    int N = 0;
    std::cin >> N;

    int e[N];
    for (int i = 0; i < N; ++i){ std::cin >> e[i]; }

    insertion_sort(e, N);
    for (int i = 0; i < N; ++i){ std::cout << " " << e[i]; }
    std::cout << std::endl;

    return 0;
}
