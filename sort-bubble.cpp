// Bubble sort
// Input: N X1 [X2 ... XN]
#include <iostream>


void bubble_sort(int x[], int N){
    bool sorted = false;
    while(!sorted){
        sorted = true;
        for (int i = 0; i < (N - 1); ++i){
            if (x[i] > x[i + 1]){
                int tmp = x[i];
                x[i]    = x[i+1];
                x[i+1]  = tmp;
                sorted  = false;
            }
        }
    }
}

int main(int argc, char* argv[]){
    int N = 0;
    std::cin >> N;

    int e[N];
    for (int i = 0; i < N; ++i){ std::cin >> e[i]; }

    bubble_sort(e, N);
    for (int i = 0; i < N; ++i){ std::cout << " " << e[i]; }
    std::cout << std::endl;

    return 0;
}
